<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('file','UploadController@index');
Route::post('file','UploadController@doUpload');
//Route::get('/', "BigcommerceController@index");
Route::get("/","MainController@index");
Route::get("/myvoice","VoiceController@voice");
Route::get("/home","MainController@index");
Route::get("/detail","MainController@detail");
//Route::get("/admin","MainController@admin");
//Route::get('/', "VoiceController@index");
Route::group(['prefix' => '/auth'], function () {
    Route::get('/install', "BigcommerceController@install");
    Route::get('uninstall', "BigcommerceController@uninstall");
    Route::get('load', "BigcommerceController@load");
});
Route::group(['prefix' => '/admin'], function () {
    Route::get('/media', "MediaController@index");
    Route::get('/chanel', "ChanelController@index");
    Route::get('/mc', "McController@index");
    Route::get('/program', "ProgranController@index");

    Route::post('/addChanel', "ChanelController@add");
    Route::post('/addMc',"McController@add");
    Route::post('/addprogram',"ProgranController@add");

    Route::get('load', "BigcommerceController@load");
});
Route::group(['prefix' => '/media'], function () {
    Route::get('/detail/{id}', "MediaController@detail");
    Route::get('/chanel', "ChanelController@index");
    Route::get('/mc', "McController@index");
    Route::get('/program', "ProgranController@index");

    Route::post('/addChanel', "ChanelController@add");
    Route::post('/addMc',"McController@add");


    Route::get('load', "BigcommerceController@load");
});
Route::get('/mediadetail/{id}', "MediaController@detail");
Route::get("/backup", "BigcommerceController@backUp");
//Route::get("/webhooks","MainController@testWebhook");
Route::get("/search", "MainController@search");
Route::get("/report", "ReportController@index");

Route::get("/ExportKeywordDetail", "ExcelController@exportKeywordDetail");
Route::get("/ExportKeywordCount", "ExcelController@exportKeywordCount");
Route::get("/result/{domain}/{type}/{keyword}/{limit?}/{page?}", "ResultController@getReult");
Route::get("/config", "ConfigController@index");
Route::get("/voice", "VoiceController@index");
Route::get("/voice/{vds}", "VoiceController@index");
Route::get("/search","MainController@search");
Route::post("/search", "MainController@searchResult");
//laf ten ham trong controller
//khi goi cai rout no goi ham index trong controler voice to text day a
