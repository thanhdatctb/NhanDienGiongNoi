-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th12 03, 2020 lúc 01:46 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `doan`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `Admin_ID` int(11) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `channel`
--

CREATE TABLE `channel` (
  `Channel_Id` int(11) NOT NULL,
  `Channel_Name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `mc`
--

CREATE TABLE `mc` (
  `MC_Id` int(11) NOT NULL,
  `MC_Name` varchar(100) NOT NULL,
  `Date_Of_Birth` date NOT NULL,
  `Adress` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `media`
--

CREATE TABLE `media` (
  `Media_Id` int(11) NOT NULL,
  `Program_Id` int(11) NOT NULL,
  `MC_Id` int(11) NOT NULL,
  `Admin_Id` int(11) NOT NULL,
  `Media_Name` varchar(100) NOT NULL,
  `Time_Start` date NOT NULL,
  `Time_End` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `program`
--

CREATE TABLE `program` (
  `Program_Id` int(11) NOT NULL,
  `Channel_Id` int(11) NOT NULL,
  `Time_Id` int(11) NOT NULL,
  `Program_Name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `segement`
--

CREATE TABLE `segement` (
  `Segement_Id` int(11) NOT NULL,
  `Media_Id` int(11) NOT NULL,
  `Time_Start` date NOT NULL,
  `Time_End` date NOT NULL,
  `Text` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `time`
--

CREATE TABLE `time` (
  `Time_Id` int(11) NOT NULL,
  `Time_Start` date NOT NULL,
  `Time_End` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Admin_ID`);

--
-- Chỉ mục cho bảng `channel`
--
ALTER TABLE `channel`
  ADD PRIMARY KEY (`Channel_Id`);

--
-- Chỉ mục cho bảng `mc`
--
ALTER TABLE `mc`
  ADD PRIMARY KEY (`MC_Id`);

--
-- Chỉ mục cho bảng `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`Media_Id`),
  ADD KEY `Admin_Id` (`Admin_Id`),
  ADD KEY `Program_Id` (`Program_Id`),
  ADD KEY `MC_Id` (`MC_Id`);

--
-- Chỉ mục cho bảng `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`Program_Id`),
  ADD KEY `Channel_Id` (`Channel_Id`),
  ADD KEY `Time_Id` (`Time_Id`);

--
-- Chỉ mục cho bảng `segement`
--
ALTER TABLE `segement`
  ADD PRIMARY KEY (`Segement_Id`),
  ADD KEY `Media_Id` (`Media_Id`);

--
-- Chỉ mục cho bảng `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`Time_Id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `admin`
--
ALTER TABLE `admin`
  MODIFY `Admin_ID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `channel`
--
ALTER TABLE `channel`
  MODIFY `Channel_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `mc`
--
ALTER TABLE `mc`
  MODIFY `MC_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `media`
--
ALTER TABLE `media`
  MODIFY `Media_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `program`
--
ALTER TABLE `program`
  MODIFY `Program_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `segement`
--
ALTER TABLE `segement`
  MODIFY `Segement_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `time`
--
ALTER TABLE `time`
  MODIFY `Time_Id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `media_ibfk_1` FOREIGN KEY (`Admin_Id`) REFERENCES `admin` (`Admin_ID`),
  ADD CONSTRAINT `media_ibfk_2` FOREIGN KEY (`Program_Id`) REFERENCES `program` (`Program_Id`),
  ADD CONSTRAINT `media_ibfk_3` FOREIGN KEY (`MC_Id`) REFERENCES `mc` (`MC_Id`);

--
-- Các ràng buộc cho bảng `program`
--
ALTER TABLE `program`
  ADD CONSTRAINT `program_ibfk_1` FOREIGN KEY (`Channel_Id`) REFERENCES `channel` (`Channel_Id`),
  ADD CONSTRAINT `program_ibfk_2` FOREIGN KEY (`Time_Id`) REFERENCES `time` (`Time_Id`);

--
-- Các ràng buộc cho bảng `segement`
--
ALTER TABLE `segement`
  ADD CONSTRAINT `segement_ibfk_1` FOREIGN KEY (`Media_Id`) REFERENCES `media` (`Media_Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
