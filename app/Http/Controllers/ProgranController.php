<?php

namespace App\Http\Controllers;

use App\Helper\ChanelHelper;
use App\Helper\ProgramHelper;
use App\Helper\TimeHelper;
use App\Model\Program;
use Illuminate\Http\Request;

class ProgranController extends Controller
{
    private $chanelHelper;
    private $programHelper;
    private $timeHelper;
    public function __construct()
    {
        $this->programHelper = new ProgramHelper();
        $this->chanelHelper = new ChanelHelper();
        $this->timeHelper = new TimeHelper();
    }
    public function index(){
        $programs = $this->programHelper->getAll();
        $chanels = $this->chanelHelper->getAll();
        $times = $this->timeHelper->getAll();
        //dd($programs)
        return view("program", [
                "programs"=>$programs,
                "chanels" => $chanels,
                "times" => $times
            ]
        );
    }
    public function add(Request $request)
    {
        $program = new Program();
        //$program->Program_Id = $request->id;
        $program->Channel_Id = $request->chanel;
        $program->Program_Name = $request->name;
        $program->Time_Id = $request->time;
        $this->programHelper->add($program);
        return $this->index();
    }
    //
}
