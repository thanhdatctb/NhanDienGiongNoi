<?php

namespace App\Http\Controllers;

use App\Helper\McHelper;
use App\Model\MC;
use Illuminate\Http\Request;

class McController extends Controller
{
    //
    private $mcHelper;
    private $segementHelper;
    public function __construct()
    {
        $this->mcHelper = new McHelper();
    }
    public function index()
    {
        $mcs = $this->mcHelper->getAll();
        return view("mc", ["mcs"=>$mcs]);
    }
    public  function  add(Request $request)
    {
        $mc = new MC();
        $mc->setMcName($request->name);
        $mc->setDateOfBirth($request->date);
        $mc->setAdress($request->address);
        $scripts =
        //dd($mc);
        $this->mcHelper->add($mc);

        return $this->index();
    }
}
