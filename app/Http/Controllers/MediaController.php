<?php

namespace App\Http\Controllers;

use App\Helper\MediaHelper;
use App\Helper\SegementHelper;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    //
    private $mediaHelper;
    private $segementHelper;
    public function __construct()
    {
        $this->mediaHelper = new MediaHelper();
        $this->segementHelper = new SegementHelper();
    }

    public function index()
    {
        $medias = $this->mediaHelper->getAll();

        return view("media",["medias"=>$medias]);
    }
    public function detail(Request $request){
        $id = $request->id;
        $media = $this->mediaHelper->getById($id);
        $segement = $this->segementHelper->getByMediaId($id);
        return view("detail",[
            "media" => $media,
            "segements" => $segement
        ]);
    }
    public  function searchResult(Request $request)
    {

    }

}
