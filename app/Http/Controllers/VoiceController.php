<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VoiceController extends Controller
{

    private   $file_mp4 = '' ;
    private   $file_mp3 = 'sample.mp3' ;
    public function __construct(){
        $this->file_mp4 =  request()->vds ;
        echo "AAAA";
        $command = "sample.exe -i ".$this->file_mp4." -q:a 0 -map a ".$this->file_mp3."";
        $output = "";
        //ffmpeg -ss 60 -i input-audio.aac -t 15 -c copy output.aac
        exec($command);
    }

    public function build_data_files($boundary, $fields, $files) {


        //$this->get_adio_from_video("","00:00:00","00:00:10");
        $namefile = "file";
        $data = '';
        $eol = "\r\n";

        $delimiter = '-------------' . $boundary;

        foreach ($fields as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $namefile . "\"" . $eol . $eol
                . $content . $eol;
        }
        foreach ($files as $name => $content) {
            $data .= "--" . $delimiter . $eol
                . 'Content-Disposition: form-data; name="' . $namefile . '"; filename="' . $name . '"' . $eol
                //. 'Content-Type: image/png'.$eol
                . 'Content-Transfer-Encoding: binary' . $eol
            ;

            $data .= $eol;
            $data .= $content . $eol;
        }
        $data .= "--" . $delimiter . "--" . $eol;


        return $data;
    }
    public function index(Request $request)
    {
        $text = "";

        $mp3file = new MP3File($this->file_mp3);
        $duration2 = $mp3file->getDuration();//(slower) for VBR (or CBR)
        $k = $duration2 / 20;
        $d = (int)$k;
        $l = $d;

        $url = "https://viettelgroup.ai/voice/api/asr/v1/rest/decode_file";
        for ($i =0; $i<$d;$i++){

            $tmp = ($i+1)*15;
            $tmp_1 = $i * 15;
            exec( "sample.exe -ss " . $tmp_1. " -i ".$this->file_mp3." -t 15 -c copy lem".$i.".mp3");

            $fields = array();

            $filenames = array("lem".$i.".mp3");

            $files = array();
            foreach ($filenames as $f) {
                $files[$f] = file_get_contents($f);
            }
            $curl = curl_init();
            $boundary = uniqid();
            $delimiter = '-------------' . $boundary;

            $post_data = $this->build_data_files($boundary, $fields, $files);


            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_SSL_VERIFYPEER => false,
                //CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POST => 1,
                CURLOPT_POSTFIELDS => $post_data,
                CURLOPT_HTTPHEADER => array(
                    //"Authorization: Bearer $TOKEN",
                    "Content-Type: multipart/form-data; boundary=" . $delimiter,
                    "Content-Length: " . strlen($post_data)
                ),
            ));


//
            $response = curl_exec($curl);
            echo $response;
            $info = curl_getinfo($curl);
//echo "code: ${info['http_code']}";
//print_r($info['request_header']);
            $obj = json_decode($response,true);
            foreach ($obj as $k){
                $tmp= $k['result']['hypotheses'];
                $text .= strval($tmp[0]['transcript'])." ";
            }


            exec("del lem".$i.".mp3");
        }
        $command = "del ".$this->file_mp3." ";
        exec( $command);
        echo $text;
        // viết đoạn insert db vào chỗ này đi
        $date = date('Y-m-d H:i:s');
        DB::table("segement")->insert(
            [
                "text" => $text,
                "media_id" => '',
                "time_start" => $date,
                "time_end" => $date,
                'media_id'=> 1
            ]

        );
        DB::table("media")->insert(
            [

                "media_id" => 1,
                "program_id" => 1,
                "mc_id" => 1,
                "admin_id"=> 1,
                "time_start" => $date,
                "time_end" => $date   ,
                "media_address" => "public/".request()->vds,
                "media_name" => request()->vds
            ]

        );


    }
    public function voice(Request $request){
        return view("view");
    }

}



// cai voice nay nhanh + dai` nen no' ra nhieu` json
// ghi am 1 doan voice tam 10s la on
