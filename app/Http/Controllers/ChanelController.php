<?php

namespace App\Http\Controllers;

use App\Helper\ChanelHelper;
use App\Helper\ChanelHelper as ChanelHelperAlias;
use App\Model\Chanel;
use Illuminate\Http\Request;

class ChanelController extends Controller
{
    //
    private  $chnelHelper ;
    public function __construct()
    {
        $this->chnelHelper = new ChanelHelper();
    }

    public  function  index(){
        $chanel = $this->chnelHelper->getAll();
//        echo("<pre>");
       //dd($chanel);
        return view("chanel", ["chanel"=>$chanel]);
    }
    public function getById($id){
        $chanel = $this->chnelHelper->getById($id)->first();
        return $chanel;
    }
    public  function add(Request $request){
        $chanel = new Chanel();
        $chanel->setChannelName($request->name);
        $this->chnelHelper->add($chanel);
        return $this->index();
    }
    public function edit(Request $request){

    }
    public function delete(Request $request){

    }
}
