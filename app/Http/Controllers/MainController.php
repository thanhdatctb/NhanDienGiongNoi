<?php

namespace App\Http\Controllers;

use App\Helper\ChanelHelper;
use App\Helper\MainHelper;
use App\Helper\McHelper;
use App\Helper\MediaHelper;
use App\Helper\ProgramHelper;
use Illuminate\Http\Request;

class MainController extends Controller
{
    //
    private $mainHelper;
    private $chanelHelper;
    private $mcHelper;
    private $programHelper;
    private $mediaHelper;
    public function __construct()
    {
        $this->mainHelper = new MainHelper();
        $this->chanelHelper = new ChanelHelper();
        $this->mcHelper = new McHelper();
        $this->programHelper = new ProgramHelper();
        $this->mediaHelper = new MediaHelper();
    }

    public function apiSearchWithoutRequest(Request $request)
    {
        $domain = MainHelper::formatDomain($request->url);
        return $this->mainHelper->searchWithoutRequest($domain, $request->keyword);
    }
//    public function search(Request $request)
//    {
//        echo "HIHI";
//    }
    public function webhook(Request $request)
    {
        $this->mainHelper->backup();
    }
    public function index(Request $request)
    {
        $chanels = $this->chanelHelper->getAll();
        $mcs = $this->mcHelper->getAll();
        $programs = $this->programHelper->getAll();
        $medias = $this->mediaHelper->getAll();
        return view("home", [
            "chanels" => $chanels,
            "programs" => $programs,
            "mcs" => $mcs,
            "medias" =>$medias
        ]);
    }
    public function detail(Request $request)
    {
        return view("detail");
    }
    public function  admin(Request $request)
    {
        return view("admincrud");
    }
    public  function  crud (Request $request)
    {
        return view("admincrud");
    }
    public function search(Request $request)
    {
        return view("search");
    }
    public function searchKeyWord(Request $request){
        $keyword = $request->keyword;
        return view("");
    }
    public  function searchResult(Request $request)
    {
        $keyword = $request->keyword;
        $medias = $this->mediaHelper->getByKeyword($keyword);
        $chanels = $this->chanelHelper->getAll();
        $mcs = $this->mcHelper->getAll();
        $programs = $this->programHelper->getAll();
        $request->session()->put("keyword",$keyword);
        return view("home", [
            "chanels" => $chanels,
            "programs" => $programs,
            "mcs" => $mcs,
            "medias" =>$medias
        ]);
    }
}
