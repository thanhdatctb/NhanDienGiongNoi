<?php


namespace App\Helper;


use Illuminate\Support\Facades\DB;

class SegementHelper
{
    public  function getByMediaId($id){
        return DB::table("segement")->where("media_id", $id)->get();
    }
}
