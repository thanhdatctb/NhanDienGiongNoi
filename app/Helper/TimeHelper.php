<?php


namespace App\Helper;


use Illuminate\Support\Facades\DB;

class TimeHelper
{
    public function getAll()
    {
        return DB::table("time")->get();

    }
}
