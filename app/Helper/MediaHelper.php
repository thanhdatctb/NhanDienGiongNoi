<?php


namespace App\Helper;


use Illuminate\Support\Facades\DB;

class MediaHelper
{
    public function getAll(){
        return DB::table("media")
            ->join("mc","mc.mc_id","media.mc_id")
            ->join("program", "program.program_id","media.program_id")
            ->get();
    }
    public function getById($id){
        return DB::table("media")
            ->join("mc","mc.mc_id","media.mc_id")
            ->where("media_id", $id)->get()->first();
    }
    public function getByKeyword($keyword){
        return DB::table("media")->
        join("segement","segement.media_id", "media.media_id")
            ->where("segement.text", "like", "%".$keyword."%")
            ->get();
    }
}
