<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MC extends Model
{
    //
    protected $table = "mc";
    public $mc_id;
    public $mc_name;
    public $date_of_birth;
    public $adress;

    /**
     * @return mixed
     */
    public function getMcId()
    {
        return $this->mc_id;
    }

    /**
     * @param mixed $mc_id
     */
    public function setMcId($mc_id): void
    {
        $this->mc_id = $mc_id;
    }

    /**
     * @return mixed
     */
    public function getMcName()
    {
        return $this->mc_name;
    }

    /**
     * @param mixed $mc_name
     */
    public function setMcName($mc_name): void
    {
        $this->mc_name = $mc_name;
    }

    /**
     * @return mixed
     */
    public function getDateOfBirth()
    {
        return $this->date_of_birth;
    }

    /**
     * @param mixed $date_of_birth
     */
    public function setDateOfBirth($date_of_birth): void
    {
        $this->date_of_birth = $date_of_birth;
    }

    /**
     * @return mixed
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * @param mixed $adress
     */
    public function setAdress($adress): void
    {
        $this->adress = $adress;
    }

}
