<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    //
    protected $table = "media";
    public $media_id;
    public $program_id;
    public $mc_id;
    public $admin_id;
    public $media_name;

    /**
     * Media constructor.
     * @param string $table
     * @param $Media_Id
     * @param $Program_Id
     * @param $MC_Id
     * @param $Admin_Id
     * @param $Media_Name
     */
    public function __construct(string $table, $Media_Id, $Program_Id, $MC_Id, $Admin_Id, $Media_Name)
    {
        $this->table = $table;
        $this->Media_Id = $Media_Id;
        $this->Program_Id = $Program_Id;
        $this->MC_Id = $MC_Id;
        $this->Admin_Id = $Admin_Id;
        $this->Media_Name = $Media_Name;
    }

    /**
     * @return mixed
     */
    public function getMediaId()
    {
        return $this->Media_Id;
    }

    /**
     * @param mixed $Media_Id
     */
    public function setMediaId($Media_Id): void
    {
        $this->Media_Id = $Media_Id;
    }

    /**
     * @return mixed
     */
    public function getProgramId()
    {
        return $this->Program_Id;
    }

    /**
     * @param mixed $Program_Id
     */
    public function setProgramId($Program_Id): void
    {
        $this->Program_Id = $Program_Id;
    }

    /**
     * @return mixed
     */
    public function getMCId()
    {
        return $this->MC_Id;
    }

    /**
     * @param mixed $MC_Id
     */
    public function setMCId($MC_Id): void
    {
        $this->MC_Id = $MC_Id;
    }

    /**
     * @return mixed
     */
    public function getAdminId()
    {
        return $this->Admin_Id;
    }

    /**
     * @param mixed $Admin_Id
     */
    public function setAdminId($Admin_Id): void
    {
        $this->Admin_Id = $Admin_Id;
    }

    /**
     * @return mixed
     */
    public function getMediaName()
    {
        return $this->Media_Name;
    }

    /**
     * @param mixed $Media_Name
     */
    public function setMediaName($Media_Name): void
    {
        $this->Media_Name = $Media_Name;
    }

}
