<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    //
    protected $table = "time";
    public $Time_Id;
    public $Time_Start;
    public $Time_End;
}
