<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    protected $table = "program";
    public $Program_Id;
    public $Channel_Id;
    public $Time_Id;
    public $Program_Name;
}
