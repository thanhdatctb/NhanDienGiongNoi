<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Chanel extends Model
{
    //
    protected $table ="channel";
    public $channel_id;
    public $channel_name;

    /**
     * @return mixed
     */
    public function getChannelId()
    {
        return $this->Channel_Id;
    }

    /**
     * @param mixed $Channel_Id
     */
    public function setChannelId($Channel_Id): void
    {
        $this->Channel_Id = $Channel_Id;
    }

    /**
     * @return mixed
     */
    public function getChannelName()
    {
        return $this->Channel_Name;
    }

    /**
     * @param mixed $Channel_Name
     */
    public function setChannelName($Channel_Name): void
    {
        $this->Channel_Name = $Channel_Name;
    }


}
