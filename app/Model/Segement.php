<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Segement extends Model
{
    //
    protected $table = "segement";
    public $Segement_Id;
    public $Media_Id;
    public $Time_Start;
    public $Time_End;
    public $Text;
}
