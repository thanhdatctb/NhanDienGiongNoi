<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="/" class="brand-link">
        <img src="/dist/img/smartosc-logo.png" alt="SmartOSC Logo" class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light">SmartOSC</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxATEBUTExIVFRUXGBsYFxYXGBgYFxcYFRcWHxcWHRgdHSogGB0lHR0YIjMiJSkrLi4uGR8zODMsNygtLisBCgoKDg0OGxAQGy8lICUtLS0vLS0vLS0tLS0tLS0tLS0tLS0vLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLf/AABEIAJwAvAMBEQACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAAABgUHAQIEAwj/xABPEAABAgMDBgcIDwYGAwAAAAABAgMABBEFEiEGBxMxQVEWIlRhcZPSFzKBkZShsdMUIzNCUlNWYnJzdJKywtEVNTZVgsElNEOio+Iks/D/xAAaAQACAwEBAAAAAAAAAAAAAAAAAwECBAUG/8QANxEAAgECAwMJBgcBAQEAAAAAAAECAxEEEiETMUEFFCIyUWFxgZEVUmKxweEzQlNyodHwNCPx/9oADAMBAAIRAxEAPwC8YACAAgAIACAAgA833koSVLUEpGJUogADeSdUTGLk7RV2Ao2pnKs9tVxormXNQQwkrqfpYA+AmNdPA1JddqP7tCjmjhGUVuzH+Xs9uXSdS5hZJpvuihHRQwzYYen+JK/7bFc8nuRk5OW697tagaB96y2BTmCqJPniVicLT6kL/u/+h0mZTm5Wr3a1J5zocKR4iTFefrhTh6E5DbuWSW1+bJ3l7/rB7SnwhH0+4ZAObGXHuc3Ot/Re/wCsT7Q7YR9PuGQ0OQs6g1ZtebG4OHSDxE080HPqb61OPkicrD2NlIxil+WmxucRcV/tujxmIc8HP8so/wC8WVvNGeH0ywaT9mvNDa617a3zk6qDwmJ5lCesKkfN2ZOftQx2HlZITfuEwhSvgHiufcVQ+GkZqmGqw1cXbt4epZST3E3CCwQAEABAAQAEABAAQAEABAAQAecw+hCStakpSkVKlEAADaSdUWjFydoq7IbsIk9l87MOFiy5czCxgp9YKWUc/P4SnmvRsjhY0+lXlbu4lM99xpL5v3plQdtObcfVrDKCUNJO7ClfAB4Yu8dGlpRivG2pGRveOFl2LLSybrDCGx81IBPSdZ8MYqmInU67fmWVNEgIUi5mC3aFwgsAUgAKQABiHEDFIlbtP5JuYIiblWkxatzISz5rjLZCF6w43xFg78MCekGNdPHV6b0d12PVFNmiF9g21Z+LLn7QYH+k6SH0j5q8b3jPMI0KeGr/AIiyy4Wso+dyOkieyZy1lJw6MEtPjvmHRdcFNdNivB5oyVcNOnrvXatxdSTGSM5YIACAAgAIACAAgAIAILKvKqXkW7zhvOK9zaTitZ1Cg3VpjGjD4aVaVlouL4FJTURTlcnZ21FB+0VKaYrebk0Epw96Vkbec46+91RqnXhhuhS1fvfcoln3j7ISLTLYbaQltA1JSAkeL+8c6pN1Hebu/Uall0OmsU6SDU2ie8nUKxF/ENQBiQ1MxIBAAQAEABAAQAEAGDAAu5T5ISs6KrTcdGKHkcVxJGo199TnrD6WLnReusezgUcMwvymUc5ZriWLSq7Lk0bnUjVU4JcGvw6/pa41OhTxKz4ffxX9EZsujLAZeStIUlQUlQBCgagg6iDtEc9pxdmMN4gAgAIACAAgAWctcrEySEoQnSzLvFZZGJUTheIGN2vj1CNOHw7qavSK4lJzykZkjketLns2eVppteOOKWQdSUjUCBtGGwbSW4rFq2ypaLdftKxjm1Y7ARg1WjG2sZIiMtloQK+VGW8tIuIbeQ6StN4aNKTgDSmKhz+KNWHwdSvHMmv5/orKpYh+65IfFTPVo7cPfJk/fj6/Yptn2GDnbkPipn7iO3F/Z00rucfX7BtX2HfYWceRmX0sp0jald5pUhIUdwIUceaEVMFVim007dmpO11sxzrGQaZrEKSe4AiQCAAgAIACAAgAxEW4ML2Oaek23W1NuIC0qBBSRUEERMZzpdKnpYq4qW8r0aexHagrds1xWIxUuWUo98Pm1Orb9LvumnDGxvop9+l/uLzZNCx5WYQ4hLjagpCgFJUk1BB1EGObKLi7Mcnc9YgAgAIAIjKm32pKWU+5jTBCa0K1mt1I/wDsACdkOoUXVnlXn4FZOyFLJCyrijaVpOIEy+eJfIQlpJSaJAUcFXa4awOlROrE1G1saCbS8/kLir6yG/8Ab8lypjrW/wBYyPD17a05ejG54rQ24QSXKmOtb7UHNa3uv0ZGZAcoJLlcv1rfag5rW91+jDMhDyinGnbes4tuIcACgq4pKhXjkA0OB2x0qKnTwc9Lav5IVJpyLJdUhKSpV1KQKkmgAA1kk6hHHTlLRbx1ktTgRbckThMy5PM43+sP2NZWTi/RlFKLdzxyjsCXnWC06OdC04KQrYpJG2CniZUalrPv7PMmcFLUW7DyhfknhJWiRjgxNakOpHvVk96oDf59Z01aEayz0vT7FYuS3jV+35LlTHWt/rGTm1aL1pv0YzaRNuEElypjrW+1Fub1vcfoyMyDhBJcqY61vtQc3re4/RhmQcIJLlTHWt9qDm9b3H6MMyDhBJcqY61vtQc3re4/RhmQcIJLlTHWt9qDm9b3H6MMyDhBJcqY61vtQc3re4/RhmQcIJLlTHWt9qDm9b3H6MMyA5QSXKmOtb/WIeHr20g/RhmR4TFrWetBQuYllJULpSXGyCDsIrE06VddLI0/BhLLIUrJdVZE4mVWomRmFHQOEk6Fw46Mk7Dv21B13jGqcViIOa6y394tPK7FjxzxwQAEAFaSo/a1ql1XGk5JV1se9cewqrcoYV6AnfHSqLm1BJdaS17UmKvmlodGeFtKmJRKhVJmmwRiKghYIw2UivJ3RlJq+64VX2EoM3Vk8kT95ztQr2hiJNrMyFQT1Z4zGQtioU2lUsgFxV1HHcxVcUqnf/BSYFiq7vaT0LbNHuc3NkckT99ztxXnlZfnZOyFS0bBlpS3bPRLtBtKgpSgCTU0WNpMdCjXnPCTza6v5IRKHSHfLtTgs6ZuJSo6JVakgBFDfPOQKkDbSOdg8nOIXXFfMfNdB+BRtiSksqUmVPJaBSPallw6bSAC4hLVfbEEnEnVjHosTL/2iop204d5kiuiWdmVmH1ySwu8W0uUaJ3FIKkg7QDt5yNkcflSEVVujRQvxHS2LHYmmi0+2HEHGhrgRqIIxB6IwQqyg7xdmNlqLXc1sfE6DVr9tcwprrxsI0+0cRuk7/yL2UTbuZ2RWmgx+tc7UR7Qrd4ZEZ7mNk8nPWOdqD2jW7WTs0Hcxsnk56xztRHtGr2hs0Hcxsnk56xztQe0avaGzQdzCyeTnrHO1E+0a3aw2aDuY2Tyc9Y52oj2jW7Q2aDuY2Tyc9Y52on2jW7WGRAc2Nk8nPWOdqJjyjWvoyHTQo5y8jZGTlEOsNXFl5KLxUpXFKVkihPNGzA4urUqSza6Mo4KHEse37FbnJNTDmpSRQ7UqAqlQ6DHMp1HSqt8ByV0Rebq2XXGlysx/mZVWjXXWtGNxznqBSvNXaIbiaSi88eq9zIg9LMb4ylxUzl20qWkFhv3Z4hloDXeXWpG3BNfDSNWEo7Spbs1Kzeh3ZHWImTk2mRrAqs/CWrFRrtimKrbSbfkRTjYW87vuUn9rb9Co18m75/tf0F1OsP8cwetxRucLLFbloI0CqJlFm4dinB36jvFQUdF7fHo8DyfFUG3vkZalTpFu5M2wiblW5hApfHGT8FQwUnwGo5wI4OIpOnU2cuBpjK6FDK3+ILN+ir88dHDf8k/F/JCp9YfbRbaU0tLtNGUkLvGibpBvVOwUrHLi5KXR38Br1QqS+SdhKSpaGpZSUYqIXeSnnVxqDwxqlicSmlLNcpkhYY7Lflyi7LqaKEUFG1JKU7hxcBGeqpud6l795eFraHTOMqW2pKVlsqBAWmhKSRrFRSo54rGSi9QaYhTGQ06VTivZ7p0qAlKSU+20boNKblEivF4orQa6xvWNpK3Qj6FHFmUZCzoXKH9ou+0pUFL4tU1CAENi7UpONb5V3qcIJYym83RWpEYMnODU1/NJrxMerjPzmCVsiL5WHBma/mk34mPVxZYmG7Zr0CzDgzNfzSb8THq4h4qK02a9AysODM1/NJvxMeriedQWmzXoGVhwZmv5pN+Jj1cHOY/pr0DKzHBma/mk14mPVwc6itNmvQjKzPBma/mk34mPVxHOY+4vQmzE3OlZDzUkhS5x98F5AuOBoJBur43EQk1oDt2xv5NqKVWSaS6L+aF1l0UWo13qegeiONNdN7xkdIoRsrh7CtGWtFODbh9jTO66vvFnopr+YmOlhXtqEqHZ0kVekrj/HOGFeW+PZdvyzGtuVbL6hsvml2v/H446NL/AMsLKrxegmTvKxYEcxbv5HPQQc73uUn9rb9Co6nJu+f7X9BFXeOlo2iywlKnXEtpUoJClGgvKrQVMc+MJSdoq466S1KAt/I+eamXEiXdcSXFFC0JUtKkqUSnECgVSlY9NhsXSVNSlNLubt/Bgqxlm0Ra2buW9hyrUq+tCZlwrd0NQVAHEg03Aa9WsRxcdJ16rqpadvA20txH5W/xBZv0Vfnh+G/5J+L+SFz6w2ZZfu6b+oc/AqMOE/6IeK+Y2fUfgUjku7OpkZ5LDTa2VI/8gqNFIBSvFPGFTTHVHoMTCjzmDk2npoZFmyjpmKA0M19Yj8Ec7ldJVEldaDqI/wCUloLl5R55ABU22pYCtVUgkVjmUaanNKXEbKdkVKM70/8AEy/3V9uO+uSsM1rmv5f0ZniBhsPOBNPSE7MKbaC5cJuBIVdN7eCSfPGDEcn0oVoRTev+7C8azsL3ddn/AImX8S+3G72RQzWu/wCP6KPEDFZWX807Zk1NltrSMqSEhIVdN4orUEk7d8YK2BjCuqae/wD3YWVd2uLvden/AImX8S+3G72RTfH+fsRzgYZPOBNqsp6cLbWkbeS2BRVwg3Kki9WvGjHPAR26pp/70J27tcXu69P/ABMv4l9uNsuSaatr/P2I5wMDecGbNkmd0bWkD+iu0Vcu8XHvq1xjDLk+O32afC/8+Bbbu1xf7rs/8TL/AHXO3G6XJFNW1/n7C3iGdWVluuzthtvupSlRmgmiKhNEhyhxJjPSw0aOKkov8v1LSneCLfa71PQPRHGm3mZo/KiHyzsoTUg+zSpUglP00cZPnAEOwtTZ1YyXH6kzXRMZB2oZmzpd0mqrlxZ3qbJSo+Eivhi2Kp5KrX+1Ig7oX8hhpbVtSYONHEspO4IqCP8AamNGJ6NGEe64tayH2Oat9xzEHO97jJ/a2/QY6fJ2+f7WJrbkdedWznH5DRthJXpUEJKgkqoFcVNSLyjjQbYrydJRrNvs/reFbqIrjJjKKeYl51rSrToWApCFiqmlaVCDS8KpFFajqO6OliKOGnKDa/N3W/30FRcsp3Zq7PmV2gibdrdUldHHVcd5RGJSDium06hC+UalONLJDt4cPEmk3caMrf4gs36Kvzxnw3/JPxfyQyfWHy00tFlwPXdEUKDl40Tcob1TsFKxzIOamsm/h4jXuFSRNgstONNuyqW3RRxIeFFihFDxtxjZKOLnJSlGTkuNmLzRSsSmSsrZzaV+wS1dJBXol3xWmFcTTCM+JlXzf+yfnf6lqaSNsuv3bN/Ur9Bi2D/Gh+5fMKnVPnaz5B59VxltbqqVuoBUQnfhHqateNN9JmGMLj1YdkzEvY9p6dlbV5KLoWmlaDGldccnEVY1MTDZ/wC9B6jaIj2fZz76illpbqgKkIBNBvNI7Eq1KD6b4dwiMLjzZ1lPsWFaAfZW2VLbIC00qLzeIrrjlTqxqY2Dg/8Aaj8toiNZ1mTD5KWGnHSkAkIBNAdVaDCsdNV6VOT2j+X1EKFx1FmPsZPTKHmltKMyggLFCRVnHHZgfFHOdWNTHRlT7PqOUbREyzrKmJgkMMuO3e+uJJpWtK0HMfFHRnXpRk9o/kKjC45TlnPMZOqQ80ptXssG6sUNCE0OOzAxgjVVTFuUPd+o7LaInWdZEzMV0DDjt00VcSSAddDQa6RveJpRXTfyEKm2OVrSDzOTzSHm1Nr9l1uqFDQhymEc6E4VMVJx936jpxtBF2Nd6OgeiPPT1k0al1UbkYRKCeqK3yLtdEmmallnBubdCBuQbhH9z4Y6OOjmlCS4xQuDsjrzTiqZ5Z1qnHa+b9YOUNNmvhRFPePlY5cXv8R0hCzve5Sf2tv0GOpydvn+1ia25HTnZs91+zrjTZcUHUqupoVUSFVoNZNCcBjFeTpxjX6W639BW6iK5ydyhfMtOB1LbxalwUqdbC1ijzYuKJxWkVBAVWhGyOliaVOTi098uD04ilJ5dxIZrkzcxaSZt2+tIQtJdV3tSB7WnZgNidW2KY+VONHIkr34fUmlJ33DPlb/ABBZv0VfnjNhv+Sfi/khk+sP85LIdbU2tIUhaSlSTqKVChHhEcuMnGSkt6HcD5+zkWazL2ipplAQgIQbo3q1x6TBVpToOUnrqYakXnLysOw5WVChLtJbCyCq7XjEDAmPPV6tSp15XNkUraHLl3+7Zv6lf4TDMJ+NDxXzK1NxD5qLHQzZ6Hbvtj/tiiddD3g6LoB6Sd8N5SrSlVcUVoxJHOP+6pr6v+4heDllrRZeotDlzY2MmXs9pV2i3QHFnab2KRXcE0Hji/KNZ1Kuj3FaUbI9M6J/wmZ6E/8AsRFcBLLiYstUWhvm6sZMtINADjuJDjiqYlSwD5gQKc0HKFV1KsrPiVpR0OLO8R+yXfpt/jTDOTZ5Kyb4ImqtCUyHsVMrJNIAopSQtw7S4sAq8A1U3AQrHVXVqyafFkUo6ELnm/dmGvTN+kxo5LnkqNv3X9Cai00GHJSx0Sso0ylNCEgrO1S1YrUT0n0RjxVSVSbdwpxSQs56v3cj7Qj8LkbOTI3qy1/K/mitfqofWu9T0D0RzW+mxkeqjcmKp7yz3Hzpl/OLatSaSnUXL3hUhMeghFTpwb91Ge9izs1wuuWi1tROLP3iaeiMePs1TfwotFWkPlI5a4+I97hBzve4yf2tv0Kjrcm2vP8AaIrbkdGdxh1VngtJWVIdQuqK3khIVVQKcRTXXZFOTcir3nusFbqFeWBb+kl5xb7LT60y6SXFVSXE6VsBLpSaOUJBrS8aUJjoYiinUjkdk5afb6cBMXLKd+bObmZm1EOrvKQ22tPFTRpoKSAEJSBdQObA76wcpU6VOjlW+/n58SaTeYZ8rf4gs36Kvzxkw3/JPxfyQ2fWLEUoAVJoI5Vm3ZDiEnsnrPmXNI6y06ugF40JoNUPjVrUo21RRxiyZAjP1tWWSSIPLwH9mTdNehXT7ph+GdqsfFFWSdjywal2mwKXG0Jp9FIEVrSzVZPvZKRDZxmyqy5lI1lFB0lSRFsO7VCWT0mwENoQNSUhI6EgCFzd5tghfzkMlyzXkDWstpH9TzY/vF8M7TIkhjZQEpCRqAoOgavNC5O8rkxFjOTK6WSDXw35dH3nkCNGDdpS8GUqIaUiMz3lo7hXzhSodZl2zqVNsA9Gkx81Y14N2z/tZDGgCMvElbhAz1/u5H2hH4XI6XJVtrP9r+aF1+qh9a70dA9Ec6W9jIvoo3hceJZ7ikLXsEzc9OOAVCZhSMPmIb/WO3OoqcIR+FCFG4xIthizbanQ+u40+lt1JopVVajgkHbf8ULdJ1sNGUdWtCJvLInznKsnlH/G52Yy8wrL8r9BjqRfEU8v8q5KcTKol3b6kzLaiLi00GI1qSN8bsHhqlPO5q2jF1ZJpWGPO6+4izwttS0qS8g3kVBTS9ieaMvJyjtnmeluPkWrXyFeWJa7LrM6t+WCnPY4K1NqLQeGmbwUlIolVaVUmlQDhtjp16M04Ri9HJWvw7LMQp9E782trPP2o0nBLSELutNi603VOHFGBUfhGpO/ZFOUKSpUXFu8m9+9+palLUZsrf4gs36K/wA8ZcN/yT8X8kMm+kNuWR/w6b+oc/AqMOEs8RDxXzGz6jKnzMzjLU0+XHENgtgArUEgm9qFcKx2OU6TkuhH0Rnoy95l0Sc8y6CWnUOAGhuKSqh3Ghwjg5JR0kmvE0Kz3M5copZTsq62BUqTSnSYmEss4vvRLRIjVFE7yZKI/KGULsupsCpUpvxBxBPmrFqckpAyQit9SUcFuSxcaCQCfbWSehDzaifEItTlaTZBICKJ6kkbbcqpwNAAmj7SzzBCgonzQ+hNRz37GVauScJT1JRGWzKqcMvQEhDyVnmCUuY+OkNozUc9+xkNElCkwtoV/nqP+HI+0I/C5HT5ISdWev5X80Jr9VEgjOPZQArMbPi3PF3uMJeCrXfRfoSpxyLUyc5VlU/zB6tzswLAVuEQlUjbeGbCWrIqeWkXph518/1roPMkROOktoorgki1Pqi1nxskkMTSR3pLS+hWKK9BvDpUI2ckVLydN+JSvG6uVLHflroY9x0WWPb2frG/xiM+Jvs2Wh1j6icQFJIIBB1ggEGuwg648jdp3R0G9CsMrsm5WXmmGmUaNFoL0DyQcAkLbXebB7xVa83NHUw1apWhJyfUWZeQl04lg2LYktKththtKBhUgcZRG1Staj0xzatepVleTGwjFITMrf4gs36K/wA8b8NJPCTXj8kKlbMP09KIdaW0vFC0lCgDQ3VChx2YRzoScJKS4DtLCirNfZXxS+tc/WNi5Qrf6/8AYl04k3k5k1KyKVpl0FIWQpQKlKxAoNZjLXrynrIbGMVuJSdmkttqcXW6kEmgKjQa6AYnoELilPQs9Be4dSW9/wAmmPVw7m012epTOZ4dSO9/yZ/1cW5tLuDOHDqR3v8Akz/q4ObS7gzhw6kd7/kz/q4ObS7gzhw6kd7/AJM/6uDm0u4M4cOpHe/5M/6uDm0u4M4cOpHe/wCTP+rg5tLuDOHDqR3v+TP+rg5tLuDOBy6kd7/kz/q4ObS7gzibnUyjl5mSShrS3g8k8Zp1tNAlYPGWgCuPnjfyXRca124+oqqyqaR3vzGRntJSqnXENoFVLUEpHOo0EROoqMXImCzM+oLKkUsMNsp71tCUD+kAVjxs5OUnJ8TopWVjnyjshE3Kuy69TiSAfgq96rwGh8EXo1XSmprgEldWPmeclltOLbcF1aFFKhuUDQ03jdHroVFOmpxdznzVmayyqOJN66ApJK6VKaEcam2muLVHmp7gjvLN4RsfKJ3yZPi9z2R57Yzv+GbG9DjnZ+SeW2ty3nFKaVebJl6FKt+CRuGyGxnVpK0aS6Wj8/kU8zs4RMfKJ7yZPYhezmn+EifMjrSds59xDjtuuqWgUSrQqSpNdxSkb4dTq1KdOUFRW98Crjd7zT/D/lBM/deiIzno9jH0Jku8xWzvlBM/deiXWk3+BH0IOyyptTLmks+01zzialyVdvjSNDvrgUcVg44Y05sDSolUVp01HvRKbTLAl8pZeas9x9t/QhKFBa6VWwpI41UbSNdNsYJYeVKpa1/qMcriRwiY+UTvkw7EbLSt+Eir8TPCJj5RO+TDsRGSf6SI8w4RMfKJ3yYdiDJP9JB5hwiY+UTvkw7EGSf6SDzDhEx8onfJh2IMk/0kHmHCJj5RO+TDsQZJ/pIPMOETHyid8mHYgyT/AEkHmHCJj5RO+TDsQZJ/pIPMwco2PlE75MOxE5J/pIPMgcsrWbdlwlNqrnCHAdEpkNhNEq4167jSurbe5o2YGMlVu4JC6jEyOtbpGdlkZmsni5MGbWOI1VKK7XCNY6BXwkRx+Vq9oqmt/E04ePEuiOAaggAqnPHkpX/z2k6gA+ANgwS74BxTzUOyOvyZisj2b4iK0Lq5VDLhSpKgBVKgoA4glJrQjdHelFyVr28DGnYchnImOSyfVK7UY/Z8XrtJ+v2GbUz3Spjkkn1R7UV5h8cvX7E7QO6XMckk+qPag5h8cvX7E7QO6XMckk+qPag5h8cvX7BtA7pcxyST6o9qDmHxy9fsG0DulTHJJPqj2oOYfHL1+wbQ2bznTSTeTKyaTvDagcee/FXydF75St4/YNoRByvfE4ZtttlpahR1CAdG8NukQSannG3Gusm3MadrXl6/YNoSvdKmOSSfVHtQcxh70/UNoHdLmOSSfVHtQcw+OXr9g2gd0uY5JJ9Ue1BzD45ev2DaB3S5jkkn1R7UHMPjl6/YNoHdLmOSSfVHtQcw+OXr9g2gd0uY5JJ9Ue1BzD45ev2DaB3S5jkkn1R7UHMPjl6/YNoHdLmOSSfVHtQcw+OXr9g2gd0qY5JJ9Ue1BzBe/L1+xG0IrKDK12baDS2ZdsBQVebQUqN0EUJrqx8whtLBqnK6lJ+LKuoRliWU7NPoYaFVLNOYDao8wEOxFbYwc3wIjHMz6SsGyW5WXQw3qQMTtUo98o85MeRrVZVZuct7OhGKirIkIWSEAGriAoFJAIIoQcQQdYiU2ndAUJnFyLVJOl1oEyyzxT8Wo/6aubcdtQNYx9HgMdtVlnvMdalbVCZHUvczhEW7yQgs+0AiLd4BBZ9oBEJPtAIleIBEtpbwCJIuEVenElahBbvAIPMAibPtC4RFu8Agt3gECTfEAibWIZ6SzC3FpQhJUpRolIxJJ1CCU4xV5biUr7i/c3uR6ZFm8uhfWOOdYSPgA+k7ac0eVxuKdeenV4G+nTUUN8YhgQAEABAB4zkq262ptxIWhQIUk4gg6xExk4u6AozLvIB2TKnWQpyXrXepuuxW9PzvHvj0eB5QhNZam8x1KNtUJEdNaiGEBFwiCwQEHRIS6FrurdS0mhJWoKUMNgCcSYibcY3SuQldjLlNYMozZ8pMy6nFl5SgVucW8EhdDoxgMR+sc/B4mVWvNS7PqPlBWNGLElm7NROuocfLjhRo0LuIaAKhVawkmpoKbOMImWInKvs1lVveCMEGU2TbbUkxPMhxtt03VMumq21G8RRVBUG6TjsIicPipTrypys7LgEoIkeCsu2xKlTK33nm76wmaaZCCaFKQlYJUaGmHweeFvF1m5JRWVX1af8AYRiiOsvJxuYtMSqm3JNJFShxQU5UCpCVEAEmuBocATsi8sXKnh9olfw3L+Qy3Z2WBk/KTz70s2w/LuISpSXFL0gBSoJAdQpAoTXZuMLrYqpSgpXi/D/6TkQmPNlKlJOtJKT/AEkiOpTeampdoiSsaQAECJCLW7Cp1WZZz0w6lplBWtWoDzknYOeF1KsKcXKbLRi5OxeWQeQjcknSOUXMEUKtiAfep/udseaxmNlXdlojbTpKA6RgGhAAQAEABAAQAYUkEUIqDrEAFa5ZZrm3Sp2TIbcOJaODajtofeE+KOpheUpU9J6oRUop7ipbTsx+XcLbzam1DYoUrzg6lDnEd+lXhVV4u5kcGnqjkhhARJARWWrsC3jDa+UDbtnSkolKwtgqKlGl03grURjtjFQw+zrzlwY2U9DxsO02WW1AqmmnFH3WXcABG5bVUpPTWsMrYd51JRT8UEZntlBlLppZqVaS4llsld5xQU64s3uMsjAUvKoK7RjhC6OEcKspvS/ZoS5ngpdlrCCWpllQHHS2pC0r50rWbyK+HoMWyVoqUFZp333vqCke9vZSpm5xt51pQbbSEJQld1d1JJrpAKhVcaimoCsFPDyp0nTXHf2a9xDkScpli2w4p5tU4+5dKW0zLiS21e1k3SS4rViaQiWEdSKi1FJdiJziYtRJJJqSSSec6/PHSgssFHsFSdzESARAbxvyTzfTc4QtSSyz8YsYqHzEaz0mgjDieUqdHox1Y6nRb3l05OZNy0k3cZRj75ZxWrpO7mGEeer4idaWabNcYqO4mIQWCAAgAIACAAgAIACAAgA47UstiYRcfaS4ncoaucHWDziL06k6bvF2IaT3lbW/miSaqlHbu5tzEeBYxHhB6Y6tHlWa0qaiJUFwK/tfJKflidLLruj36ReT95Orw0jq0sbRqbnr36GeVKSISsafMpZhBmXYARF+8toEHmGgRHmGgQX7yNAibNbyNT3lJJ11V1ptbityElR80VlWpxV5MlQb3DnYma2feoXbrCfnGq/uDb0kRz63KtKOkNWOjh295ZOTmbyQlaKuaZwe/cxp9FOpPp545FbHVqujdl2I0RpRiNsYxgQAEABAAQAEABAAQAEABAAQAEABABikAEVaOTUi/wC6yzSz8K6Ar7wofPDYV6kOrJlXFPehbns1lmKBKUut68EOdoGNlPlOvHR2fiijpRZXOUOSrDBNxbhp8IpP5Y6VHGTnvSEujFCdMG6qgjfGpdbkV2aJGzJBDp4xI6Kf3BhNSu47kiVSRZGTebaSeTeWt/VqCkAfgr545lblKrF6Jen3GRoRG+Qze2W1iJYLO9wqX5iaeaMM8fiJ75DVTiuAxy0q22LraEoG5KQkeIRlcm9WXse0QAQAEABAAQAEABAAQAf/2Q==" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">Admin</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                    <a href="#" class="nav-link active">
                        <i class="nav-icon fas fas fa-search"></i>
                        <p>
                            Advanced search
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a  href="/" class="nav-link" >
{{--                        <i class="nav-icon fas fa-sync"></i>--}}
                        <i class="nav-icon fas fa-home"></i>
                        <p>
                            Home
                        </p>
                    </a>
                </li>
                <li class="nav-item" id="synLink">
                    <a  href="#" class="nav-link" >
                        <i class="nav-icon fas fa-sync"></i>
                        <p>
                            Synchronize
                            <span class="right badge badge-danger">For fist time</span>
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/report" class="nav-link">
                        <i class="nav-icon fas fa-file-alt"></i>
                        <p>Report</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/admin/chanel" class="nav-link">
                        <i class="nav-icon fas fa-tv"></i>
                        <p>Chanel</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/admin/mc" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>MC</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/admin/program" class="nav-link">
                        <i class="nav-icon fa fa-television"></i>
                        <p>Program</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/admin/media" class="nav-link">

                        <i class=" nav-icon fas fa-video"></i>
                        <p>Media</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-file-export"></i>
                        <p>
                            Export
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">2</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="/ExportKeywordDetail" class="nav-link">
                                <i class="fas fa-arrow-circle-down nav-icon"></i>
                                <p>Keyword search detail</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="/ExportKeywordCount" class="nav-link">
                                <i class="fas fa-arrow-circle-down nav-icon"></i>
                                <p>Keyword search count</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="/config" class="nav-link">
                        <i class="nav-icon fas fa-cog" aria-hidden="true"></i>
                        <p>Configuration</p>
                    </a>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link">
                        <i class=" nav-icon fa fa-user" aria-hidden="true"></i>
                        <p>About us</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
<script type="text/javascript">
    $(document).ready(function(e) {

       $("#synLink").click(function () {
           alert("Waiting for few second");
           synAjax();
       })
    });
    function synAjax()
    {
        $.ajax({
            url: "/backup",
            type: "GET",
            success: function f(result){
                alert("Your data is backing up");
            }
        })
    }

</script>
